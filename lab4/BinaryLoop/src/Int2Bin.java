
public class Int2Bin {
	public static void main (String[] args ) {
		System.out.println(int2Bin(12));
		
		
	}
	
	private static String int2Bin(int number) {
		if (number==0 || number==1)
			return number + "";
		return int2Bin(number/2) + (number%2);
		
	}

}
