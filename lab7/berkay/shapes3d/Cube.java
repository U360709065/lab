package berkay.shapes3d;

import berkay.shapes.Square;

public class Cube extends Square {
	
private int height ;
	
	public Cube (int height,int width, int length) {
		super(width, length) ;
		this.height = height ;
		
	}
	
	public int area() {
		int area ;
		area = (super.area() + (width * height) + (height * length)) * 2 ;
		return area ;
	}
	
	public int volume() {
		return super.area() * height ;
	}
	
	public String toString() {
		return "Width = " + super.width + ", Length = " + super.length + ", Height = " + height ;
	}

}