package berkay.shapes3d;

import berkay.shapes.Circle;

public class Cylinder extends Circle {
	protected int height;
	
	 	
	public Cylinder () {
		this(5,10);
		System.out.println("Cylinder is being created");
	}
		
	public Cylinder (int r) {
			this (r,10);
			System.out.println("Cylinder is being created");
	}

	public Cylinder (int r,int h) {
		super(5);
		height =h;
		System.out.println("Cylinder is being created with r="+r);

	}
	
	public double area() {
		return 2 * super.area()+
		  2*Math.PI*radius*height;
		
	}
	
	public double volume() {
		return super.area()*height;
	}
	
	public String toString () {
		return "radius =" + radius+
				", height =" + height;
		
	}
}